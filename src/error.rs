use super::models::FeedlyError;
use failure::{Backtrace, Context, Fail};
use std::fmt;

#[derive(Debug)]
pub struct ApiError {
    inner: Context<ApiErrorKind>,
}

#[derive(Clone, Eq, PartialEq, Debug, Fail)]
pub enum ApiErrorKind {
    #[fail(display = "Url Error")]
    Url,
    #[fail(display = "Json Error")]
    Json,
    #[fail(display = "Http request failed")]
    Http,
    #[fail(display = "Feedly error")]
    Feedly(FeedlyError),
    #[fail(display = "Malformed input arguments")]
    Input,
    #[fail(display = "IO Error")]
    IO,
    #[fail(display = "No valid access token available")]
    Token,
    #[fail(display = "Request failed with message access denied")]
    AccessDenied,
    #[fail(display = "Internal error")]
    InternalMutabilty,
    #[fail(display = "Access token expired")]
    TokenExpired,
    #[fail(display = "Unknown error")]
    Unknown,
}

impl Fail for ApiError {
    fn cause(&self) -> Option<&dyn Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.inner, f)
    }
}

impl ApiError {
    pub fn kind(&self) -> ApiErrorKind {
        self.inner.get_context().clone()
    }

    pub fn parse_feedly_error(error: FeedlyError) -> Self {
        if error.error_message.starts_with("token expired") {
            ApiErrorKind::TokenExpired.into()
        } else {
            ApiErrorKind::Feedly(error).into()
        }
    }
}

impl From<ApiErrorKind> for ApiError {
    fn from(kind: ApiErrorKind) -> ApiError {
        ApiError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<ApiErrorKind>> for ApiError {
    fn from(inner: Context<ApiErrorKind>) -> ApiError {
        ApiError { inner }
    }
}
