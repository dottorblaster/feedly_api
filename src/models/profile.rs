use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Profile {
    pub id: String,
    #[serde(default)]
    pub email: Option<String>,
    #[serde(default)]
    pub given_name: Option<String>,
    #[serde(default)]
    pub family_name: Option<String>,
    #[serde(default)]
    pub picture: Option<String>,
    #[serde(default)]
    pub gender: Option<String>,
    #[serde(default)]
    pub locale: Option<String>,
    #[serde(default)]
    pub google: Option<String>,
    #[serde(default)]
    pub reader: Option<String>,
    #[serde(default)]
    pub twitter_user_id: Option<String>,
    #[serde(default)]
    pub facebook_user_id: Option<String>,
    #[serde(default)]
    pub word_press_id: Option<String>,
    #[serde(default)]
    pub windows_live_id: Option<String>,
    pub wave: Option<String>,
    pub client: Option<String>,
    #[serde(default)]
    pub source: Option<String>,
    #[serde(default)]
    pub created: Option<u64>,
}

impl Profile {
    #[allow(clippy::type_complexity)]
    pub fn decompose(
        self,
    ) -> (
        String,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<String>,
        Option<u64>,
    ) {
        (
            self.id,
            self.email,
            self.given_name,
            self.family_name,
            self.picture,
            self.gender,
            self.locale,
            self.google,
            self.reader,
            self.twitter_user_id,
            self.facebook_user_id,
            self.word_press_id,
            self.windows_live_id,
            self.wave,
            self.client,
            self.source,
            self.created,
        )
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProfileUpdate {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub given_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub family_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub picture: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub gender: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub locale: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub twitter: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub facebook: Option<String>,
}
