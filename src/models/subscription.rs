use super::category::Category;
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Subscription {
    pub id: String,
    pub title: Option<String>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub categories: Option<Vec<Category>>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub website: Option<String>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub updated: Option<u64>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscribers: Option<u64>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub velocity: Option<f32>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub topics: Option<Vec<String>>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "contentType")]
    pub content_type: Option<String>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconUrl")]
    pub icon_url: Option<String>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub partial: Option<bool>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "sortid")]
    pub sort_id: Option<String>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub added: Option<u64>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "visualUrl")]
    pub visual_url: Option<String>,
}

impl Subscription {
    #[allow(clippy::type_complexity)]
    pub fn decompose(
        self,
    ) -> (
        String,
        Option<String>,
        Option<Vec<Category>>,
        Option<String>,
        Option<u64>,
        Option<u64>,
        Option<f32>,
        Option<Vec<String>>,
        Option<String>,
        Option<String>,
        Option<bool>,
        Option<String>,
        Option<u64>,
        Option<String>,
    ) {
        (
            self.id,
            self.title,
            self.categories,
            self.website,
            self.updated,
            self.subscribers,
            self.velocity,
            self.topics,
            self.content_type,
            self.icon_url,
            self.partial,
            self.sort_id,
            self.added,
            self.visual_url,
        )
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct SubscriptionInput {
    pub id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub categories: Option<Vec<Category>>,
}
